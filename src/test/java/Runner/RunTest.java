package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.*;

@RunWith(Cucumber.class)
@CucumberOptions(features ="src/test/java/features/login.feature", glue="steps", monochrome=true, dryRun=false, 
snippets=SnippetType.CAMELCASE)
public class RunTest {
	
	
	
}

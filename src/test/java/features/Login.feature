Feature: Login for Leaftaps

Background:
Given Open chrome browser
And Maximize the browser
And Set the timeout
And Enter the url

Scenario: Positive flow

And Enter the username as DemoSalesManager
And Enter the password as crmsfa
When Click on login button
Then Verify login is success

Scenario: Positive flow

And Enter the username as DemoCSR
And Enter the password as crmsfa
When Click on login button
Then Verify login is success

Scenario: Create lead

And Enter the username as DemoCSR
And Enter the password as crmsfa
When Click on login button
Then Verify login is success
When Click on CRM/SFA 
Then Verify MyHomepage is loaded
When Click on Leads
Then Verify Lead is loaded
When Click on create lead
Then Verify created lead page is loaded
And Enter company name
And Enter first name
And Enter last name
When Click on Create lead button
Then Verify a create lead is created 

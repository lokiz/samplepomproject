package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	
	public ChromeDriver driver;

	@When("Click on CRM/SFA")
	public void clickOnCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("Verify MyHomepage is loaded")
	public void verifyMyHomepageIsLoaded() {
		System.out.println("My home page is loaded");
	}

	@When("Click on Leads")
	public void clickOnLeads() {
		driver.findElementByLinkText("Leads").click();
	}

	@Then("Verify Lead is loaded")
	public void verifyLeadIsLoaded() {
	   System.out.println("Leads page is loaded");
	}

	@When("Click on create lead")
	public void clickOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Verify created lead page is loaded")
	public void verifyCreatedLeadPageIsLoaded() {
	   System.out.println("Create lead page is loaded");
	}

	@Then("Enter company name")
	public void enterCompanyName() {
		driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementById("createLeadForm_companyName").sendKeys("Flipkart");
	}

	@Then("Enter first name")
	public void enterFirstName() {
		driver.findElementById("createLeadForm_firstName").clear();
		driver.findElementById("createLeadForm_firstName").sendKeys("Spider");
	}

	@Then("Enter last name")
	public void enterLastName() {
		driver.findElementById("createLeadForm_lastName").clear();
		driver.findElementById("createLeadForm_lastName").sendKeys("Man");
	}

	@When("Click on Create lead button")
	public void clickOnCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify a create lead is created")
	public void verifyACreateLeadIsCreated() {
	    System.out.println("New lead is created");
	}

}

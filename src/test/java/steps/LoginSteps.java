package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	public ChromeDriver driver;
	
	@Given("Open chrome browser")
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
	}
	
	@And("Maximize the browser")
	public void maxWindow() {
		driver.manage().window().maximize();
	}
	
	@And("Set the timeout")
	public void setTimeOut() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@And("Enter the url")
	public void enterUrl() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the username as (.*)")
	public void enterUserName(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}
	
	@And("Enter the password as (.*)")
	public void enterPassword(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}
	
	@When("Click on login button")
	public void clickLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@Then("Verify login is success")
	public void verifyLogin() {
		System.out.println("Logged in successfully");
	}
}
